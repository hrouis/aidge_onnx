#!/usr/bin/env python3
""" Aidge ONNX

#TODO To change
POC of the next framework named Aidge
"""


import sys
import os
import shutil
import pathlib
import subprocess
import multiprocessing

from math import ceil

from setuptools import setup, Extension
from setuptools import find_packages
from setuptools.command.build_ext import build_ext

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
"""
DOCLINES = (__doc__ or '').split("\n")
ROOT_PACKAGE = "aidge_onnx"

def get_version() -> str:
    return open(pathlib.Path().absolute() / "version.txt", "r").read().strip()


if __name__ == '__main__':

    setup(
        name='aidge_onnx',
        version=get_version(),
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        packages=[ROOT_PACKAGE] + [f"{ROOT_PACKAGE}.{item}" for item in find_packages(where=ROOT_PACKAGE)],
        install_requires=['aidge_core'],
    )
