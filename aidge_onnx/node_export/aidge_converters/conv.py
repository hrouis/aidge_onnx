"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import aidge_core
import onnx
from onnx import helper
from aidge_onnx.node_export import auto_register_export

@auto_register_export("Conv")
def export_conv(
    aidge_node: aidge_core.Node,
    node_inputs_name,
    node_outputs_name,
    opset:int = None,
    verbose: bool = False,
    **kwargs) -> None:

    aidge_operator = aidge_node.get_operator()
    #If bias not set, remove bias as an input
    if aidge_node.input(2)[0] is not None and\
          not aidge_node.input(2)[0].get_operator().get_output(0).has_impl():
        node_inputs_name.remove(aidge_node.input(2)[0].name()+"_out0")
    onnx_node = helper.make_node(
        name=aidge_node.name(),
        op_type=aidge_node.type(),
        inputs=node_inputs_name,
        outputs=node_outputs_name,
    )
    onnx_node.attribute.append(
        helper.make_attribute(
            "dilations",
            aidge_operator.get_attr("DilationDims")
    ))
    onnx_node.attribute.append(
        helper.make_attribute(
            "group",
            1
    ))
    onnx_node.attribute.append(
        helper.make_attribute(
            "kernel_shape",
            aidge_operator.get_attr("KernelDims")
    ))
    onnx_node.attribute.append(
        helper.make_attribute(
            "strides",
            aidge_operator.get_attr("StrideDims")
    ))
    return [onnx_node]
