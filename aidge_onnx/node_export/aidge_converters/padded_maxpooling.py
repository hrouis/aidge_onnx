"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import aidge_core
import onnx
from onnx import helper
from aidge_onnx.node_export import auto_register_export

@auto_register_export("PaddedMaxPooling")
def export_maxpooling(
    aidge_node: aidge_core.Node,
    node_inputs_name,
    node_outputs_name,
    opset:int = None,
    verbose: bool = False,
    **kwargs) -> None:
    aidge_operator = aidge_node.get_operator()

    micro_graph = aidge_operator.get_micro_graph()
    maxpool_op, pad_op = None, None
    for node in micro_graph.get_nodes():
        if node.type() == "MaxPooling":
            maxpool_op = node.get_operator()
        elif node.type() == "Pad":
            pad_op = node.get_operator()
        else:
            raise RuntimeError(f"Unsupported node type: {node.type()} inside PaddedMaxPooling.")

    # Computing padding
    kernel_dims = maxpool_op.get_attr("KernelDims")
    aidge_pads  = pad_op.get_attr("BeginEndBorders")
    pads = [0] * 2*len(kernel_dims)
    for i in range(0, len(kernel_dims)):
        pads[i] = aidge_pads[2*i]
        pads[len(kernel_dims)+i] = aidge_pads[2*i+1]

    onnx_node = helper.make_node(
        name=aidge_node.name(),
        op_type="MaxPool",
        inputs=node_inputs_name,
        outputs=node_outputs_name,
    )

    onnx_node.attribute.append(
        helper.make_attribute(
            "kernel_shape",
            maxpool_op.get_attr("KernelDims")
    ))
    onnx_node.attribute.append(
        helper.make_attribute(
            "strides",
            maxpool_op.get_attr("StrideDims")
    ))
    onnx_node.attribute.append(
        helper.make_attribute(
            "pads",
            pads
    ))
    return [onnx_node]
