"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import

@auto_register_import("slice")
def import_slice(onnx_node:onnx.NodeProto, input_nodes:List[aidge_core.Node], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    """
    node_name = onnx_node.output[0]

    for attr in onnx_node.attribute:
        print(f"Warning: Attribute {attr.name} is not supported for operator slice.")
        return None

    axes = input_nodes.pop().get_operator().get_output(0)
    ends = input_nodes.pop().get_operator().get_output(0)
    ends = [i - 1 for i in ends]
    starts = input_nodes.pop().get_operator().get_output(0)

    slice_node = aidge_core.Slice( starts, ends, axes, name=node_name)

    print(f"- {node_name} ({onnx_node.op_type})")
    return slice_node
