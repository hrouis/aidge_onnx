import onnx
import numpy as np

_AIDGE_DOMAIN = "ai.onnx.converters.aidge"

_MAP_NP_ONNX_TYPE = {
        np.dtype(np.float32): onnx.TensorProto.FLOAT,
        np.dtype(np.float64): onnx.TensorProto.DOUBLE,
        np.dtype(np.int8): onnx.TensorProto.INT8,
        np.dtype(np.int16): onnx.TensorProto.INT16,
        np.dtype(np.int32): onnx.TensorProto.INT32,
        np.dtype(np.int64): onnx.TensorProto.INT64,
        np.dtype(np.uint8): onnx.TensorProto.UINT8,
        np.dtype(np.uint16): onnx.TensorProto.UINT16,
        np.dtype(np.uint32): onnx.TensorProto.UINT32,
        np.dtype(np.uint64): onnx.TensorProto.UINT64,
        np.dtype(np.bool_): onnx.TensorProto.BOOL,
    }
_MAP_ONNX_NP_TYPE = {v: k for k, v in _MAP_NP_ONNX_TYPE.items()}

def replace_with_underscore(input_string):
    # Use the replace method to replace '/' with '_'
    modified_string = input_string.replace('/', '_')
    modified_string = modified_string.replace('.', '_')
    return modified_string

def replace_digit_first_character(input_string):
    modified_string = input_string
    if len(input_string) > 0 and input_string[0].isdigit():
        modified_string = "data_" + input_string
    return modified_string

def replace(name):
    # All replacements to do in the name
    new_name = replace_with_underscore(name)
    new_name = replace_digit_first_character(new_name)
    return new_name

def clean_names(model: onnx.ModelProto):
    for i in model.graph.initializer:
        i.name = replace(i.name)

    for n in model.graph.node:
        if len(n.name) > 0 and n.name[0].isdigit():
            new_name = "layer_" + n.name
            n.name = new_name
        for index, i in enumerate(n.input):
            n.input[index] = replace(i)
        for index, o in enumerate(n.output):
            n.output[index] = replace(o)

    for i in model.graph.input:
        i.name = replace(i.name)

    for o in model.graph.output:
        o.name = replace(o.name)

    return model

def clean2aidge(model:onnx.ModelProto):
    model = clean_names(model)
    return model

def numpy_to_onnx_type(np_dtype):
    if np_dtype not in _MAP_NP_ONNX_TYPE:
        raise ValueError(f"Unsupported NumPy dtype: {np_dtype}")
    onnx_type = _MAP_NP_ONNX_TYPE[np_dtype]
    return onnx_type

def onnx_to_numpy_type(onnx_type):
    if onnx_type not in _MAP_ONNX_NP_TYPE:
        raise ValueError(f"Unsupported ONNX TensorProto type: {onnx_type}")
    np_dtype = _MAP_ONNX_NP_TYPE[onnx_type]
    return np_dtype
